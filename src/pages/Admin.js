import { useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";



export default function AdminDashboard(){

	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

	// Create allCourses state to contain the courses from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);

	//USER
	const [isAdmin, setIsAdmin] = useState("");
	const [userId, setUserId] = useState("");

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add course modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal

	

	// To control the edit course modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific course and bind it with our input fields.
	const openEdit = (id) => {
		console.log(id);
		setProductId(id);

		// Getting a specific course to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {
			
			console.log(data);

			// updating the course states for editing
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setProductName('');
	    setDescription('');
	    setPrice(0);
	    setStocks(0);

		setShowEdit(false);
	};

	// [SECTION] To view all course in the database (active & inactive)
	// fetchData() function to get all the active/inactive courses.
	const fetchData = () =>{
		// get all the courses from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/getAll`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive? "Active": "InActive"}</td>
						
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the course
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.productName)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.productName)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}
// to fetch all courses in the first render of the page.
useEffect(()=>{
	fetchData();
}, [])

// [SECTION] Setting the course to Active/Inactive

// Making the course inactive
const archive = (id, productName) =>{
	console.log(id);
	console.log(productName);

	// Using the fetch method to set the isActive property of the course document to false
	fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
		method: "PATCH",
		headers:{
			"Content-Type": "application/json",
			"Authorization": `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			isActive: false
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(data){
			Swal.fire({
				title: "Archive Successful",
				icon: "success",
				text: `${productName} is now inactive.`
			});
			// To show the update with the specific operation intiated.
			fetchData();
		}
		else{
			Swal.fire({
				title: "Archive unsuccessful",
				icon: "error",
				text: "Something went wrong. Please try again later!"
			});
		}
	})
}

// Making the course active
const unarchive = (id, productName) =>{
	console.log(id);
	console.log(productName);

	// Using the fetch method to set the isActive property of the course document to false
	fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
		method: "PATCH",
		headers:{
			"Content-Type": "application/json",
			"Authorization": `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			isActive: true
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		if(data){
			Swal.fire({
				title: "Unarchive Successful",
				icon: "success",
				text: `${productName} is now active.`
			});
			// To show the update with the specific operation intiated.
			fetchData();
		}
		else{
			Swal.fire({
				title: "Unarchive Unsuccessful",
				icon: "error",
				text: "Something went wrong. Please try again later!"
			});
		}
	})
}

// [SECTION] Adding a new course
// Inserting a new course in our database
const addProduct = (e) =>{
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Product succesfully Added",
					icon: "success",
					text: `${productName} is now added`
				});

				// To automatically add the update in the page
				fetchData();
				// Automatically closed the modal
				closeAdd();
			}
			else{
				Swal.fire({
					title: "Error!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				});
				closeAdd();
			}

		})

		// Clear input fields
		setProductName('');
		setDescription('');
		setPrice(0);
		setStocks(0);
}

// [SECTION] Edit a specific course
// Updating a specific course in our database
// edit a specific course

const updateProduct = (e) =>{
		// Prevents page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stocks: stocks
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Course succesfully Updated",
					icon: "success",
					text: `${productName} is now updated`
				});

				// To automatically add the update in the page
				fetchData();
				// Automatically closed the form
				closeEdit();

			}
			else{
				Swal.fire({
					title: "Error!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				});

				closeEdit();
			}

		})

		// Clear input fields
		setProductName('');
		setDescription('');
		setPrice(0);
		setStocks(0);
} 

// Submit button validation for add/edit course
useEffect(() => {

	// Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
	if(productName !== "" && description !== "" && price > 0 && stocks > 0){
		setIsActive(true);
	} else {
		setIsActive(false);
	}

}, [productName, description, price, stocks]);

//*ALL USERS FUNCTIONS (stretch Goal)

const { user } = useContext(UserContext);

const [allUsers, setAllUsers] = useState([]);


const userData = () =>{
	// get all the courses from the database
	fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
		headers:{
			"Authorization": `Bearer ${localStorage.getItem("token")}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		setAllUsers(data.map(user => {
			return (
				<tr key={user._id}>
					<td>{user._id}</td>
					<td>{user.firstName}</td>
					<td>{user.lastName}</td>
					<td>{user.email}</td>
					<td>{user.mobileNo}</td>
					<td>{user.isAdmin? "Yes" : "No"}</td>
					
					<td>	
									<Button variant="info" size="sm" className="mx-1" >Promote</Button>
								
						
					</td>
				</tr>
			)
		}));
	});
}



useEffect(() =>{
	userData();
}, [])



const updateUser = () =>{
	// Prevents page redirection via form submission
	

	fetch(`${process.env.REACT_APP_API_URL}/users/update/${userId}`, {
		method: "PATCH",
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			isAdmin:isAdmin
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(data){
			Swal.fire({
				title: "Status succesfully Updated",
				icon: "success",
				text: "account is updated to Admin"
			});

			// To automatically add the update in the page
			fetchData();
			// Automatically closed the form
			closeEdit();

		}
		else{
			Swal.fire({
				title: "Error!",
				icon: "error",
				text: `Something went wrong. Please try again later!`
			});

			closeEdit();
		}

	})

	
} 





return(
	
	(userRole)?

		
	

	<>
		<div className="mt-5 mb-3 text-center">
			<h1>Admin Dashboard</h1>
			
			<Button variant="success" className="
			mx-2" onClick={openAdd}>Add Product</Button>
			
			<Button variant="secondary" className="
			mx-2"> Show Products</Button>
		</div>
		
		<Table striped bordered hover>
		<thead>
			<tr>
		<th>Product ID</th>
			<th>Product Name</th>
			<th>Description</th>
			<th>Price</th>
			<th>Stocks</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		</thead>
		<tbody>
			{allProducts}
		</tbody>
		</Table>
		
		<Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
			<Form onSubmit={e => addProduct(e)}>

				<Modal.Header closeButton>
					<Modal.Title>Add New Product</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<Form.Group controlId="name" className="mb-3">
						<Form.Label>Product Name</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter Product Name" 
							value = {productName}
							onChange={e => setProductName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="description" className="mb-3">
						<Form.Label>Product Description</Form.Label>
						<Form.Control
							as="textarea"
							rows={3}
							placeholder="Enter Product Description" 
							value = {description}
							onChange={e => setDescription(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="price" className="mb-3">
						<Form.Label>Product Price</Form.Label>
						<Form.Control 
							type="number" 
							placeholder="Enter Product Price" 
							value = {price}
							onChange={e => setPrice(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="stocks" className="mb-3">
						<Form.Label>Product Stocks</Form.Label>
						<Form.Control 
							type="number" 
							placeholder="Enter Course Slots" 
							value = {stocks}
							onChange={e => setStocks(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="name" className="mb-3">
						<Form.Label>Status</Form.Label>
						<Form.Control 
							type="text" 
							placeholder="Enter Status" 
							value = {isActive}
							onChange={e => setIsActive(e.target.value)}
							required
						/>
					</Form.Group>

					
				</Modal.Body>

				<Modal.Footer>
					{ isActive 
						? 
						<Button variant="primary" type="submit" id="submitBtn">
							Save
						</Button>
						: 
						<Button variant="danger" type="submit" id="submitBtn" disabled>
							Save
						</Button>
					}
					<Button variant="secondary" onClick={closeAdd}>
						Close
					</Button>
				</Modal.Footer>

			</Form>	
		</Modal>
	
	<Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
		<Form onSubmit={e => updateProduct(e)}>

			<Modal.Header closeButton>
				<Modal.Title>Edit a Product</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<Form.Group controlId="name" className="mb-3">
					<Form.Label>Product Name</Form.Label>
					<Form.Control 
						type="text" 
						placeholder="Enter Product Name" 
						value = {productName}
						onChange={e => setProductName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="description" className="mb-3">
					<Form.Label>Product Description</Form.Label>
					<Form.Control
						as="textarea"
						rows={3}
						placeholder="Enter Product Description" 
						value = {description}
						onChange={e => setDescription(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="price" className="mb-3">
					<Form.Label>Product Price</Form.Label>
					<Form.Control 
						type="number" 
						placeholder="Enter Product Price" 
						value = {price}
						onChange={e => setPrice(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="stocks" className="mb-3">
					<Form.Label>Product Stocks</Form.Label>
					<Form.Control 
						type="number" 
						placeholder="Enter Product Stocks" 
						value = {stocks}
						onChange={e => setStocks(e.target.value)}
						required
					/>
				</Form.Group>
			</Modal.Body>

			<Modal.Footer>
				{ isActive 
					? 
					<Button variant="primary" type="submit" id="submitBtn">
						Save
					</Button>
					: 
					<Button variant="danger" type="submit" id="submitBtn" disabled>
						Save
					</Button>
				}
				<Button variant="secondary" onClick={closeEdit}>
					Close
				</Button>
			</Modal.Footer>

		</Form>	
		
	</Modal>
			
			<h1 className="text-center">Users Data</h1>
	<Table striped bordered hover>
		  <thead>
			<tr>
			  <th>User ID</th>
			  <th>First Name</th>
			  <th>Last Name</th>
			  <th>Email</th>
			  <th>mobileNo</th>
			  <th>isAdmin</th>
			  
			  <th>Actions</th>
			</tr>
		  </thead>
		  <tbody>
			{allUsers}
		  </tbody>
		</Table>




</>
	:

	<Navigate to="/" />
	
	
)
}


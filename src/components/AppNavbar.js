import { Fragment, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import UserContext from '../UserContext';




export default function AppNavbar() {


  const { user } = useContext(UserContext);
  console.log(user);
  const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
  console.log(userRole);

  const [userId, setUserId] = useState(localStorage.getItem("userId"));

  return (
    
      <Navbar variant='dark' className='navbar'>
      <Navbar.Brand className='logo' href="#home">EmShop</Navbar.Brand>
        
          
          <Nav className="m-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/Products">Products</Nav.Link>
            
            {(user.isAdmin !== false  && userRole !== false)? 
              <Fragment>
              <Nav.Link as={NavLink} to="/Transactions">Transactions</Nav.Link>
                <Nav.Link as={NavLink} to="/Admin">Admin Dashboard</Nav.Link>
              </Fragment>
              :
              <>
              </> 
            }
            {(user.id == null && userId == null)?
              <Fragment>  
            <Nav.Link as={NavLink} to="/Register">Register</Nav.Link>
            <Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
          </Fragment>
              
              :
              <Nav.Link as={NavLink} to="/Logout">Logout</Nav.Link> 
            }
            
         
            
              
          </Nav>
          
       
      </Navbar>
      
  );
}


import {Card, Button} from 'react-bootstrap';
import  PropTypes  from 'prop-types';
import  { Link } from 'react-router-dom';


export default ProductsCard;


function ProductsCard({productProp}) {

  const {_id, productName, description, price, stocks} = productProp;

  return (
   
    <Card className="productcard">
     
    <Card.Body>
        <Card.Title>
        {productName}
        </Card.Title>

        <Card.Subtitle>
            Description:
        </Card.Subtitle>

        <Card.Text>
        {description}
        </Card.Text>

        <Card.Text>
        Price:
        </Card.Text>
        <Card.Text className='prodtext'>
        {price}
        </Card.Text>

        <Card.Text>
        Stocks:
        </Card.Text>

        <Card.Text className='prodtext'>
        {stocks}
        </Card.Text>
        

        <Button as={Link} to ={`/products/${_id}`} className="prodbutton">Add to Cart</Button>
      

    </Card.Body>
    </Card>
        
    
)
}